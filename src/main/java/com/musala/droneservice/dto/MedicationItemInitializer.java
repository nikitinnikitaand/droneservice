package com.musala.droneservice.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * Initializer representation of medication item. Contains only key data.
 */
@Data
@Getter
@Setter
public class MedicationItemInitializer {
    private String name;
    private long weight;
    private String code;
}

package com.musala.droneservice.model;

import lombok.*;

import javax.persistence.*;

/**
 * Medication item representation in database.
 */
@Entity
@Table(name = "medication_item")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class MedicationItemEntity {
    /**
     * Identifier of medication.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "medication_id")
    private long medicationId;

    /**
     * Medication item's name.
     */
    @Column(name = "name")
    private String name;

    /**
     * Medication item's total weight.
     */
    @Column(name = "weight")
    private long weight;

    /**
     * Medication item's code.
     */
    @Column(name = "code")
    private String code;

    /**
     * Corresponding image of current medication item.
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "image_id", referencedColumnName = "image_id")
    private ImageEntity image;

    /**
     * Corresponding drone to which the current medication relates.
     */
    @ManyToOne
    @JoinColumn(name = "drone_id")
    private DroneEntity drone;

    public MedicationItemEntity(String name, long weight, String code, DroneEntity drone) {
        this.name = name;
        this.weight = weight;
        this.code = code;
        this.drone = drone;
    }

    public MedicationItemEntity(String name, long weight, String code) {
        this.name = name;
        this.weight = weight;
        this.code = code;
    }
}

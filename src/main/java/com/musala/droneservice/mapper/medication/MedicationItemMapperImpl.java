package com.musala.droneservice.mapper.medication;

import com.musala.droneservice.dto.MedicationItem;
import com.musala.droneservice.model.MedicationItemEntity;
import org.springframework.stereotype.Service;

/**
 * Implementation of service to convert database entities to DTO and vice versa for medication items.
 */
@Service
public class MedicationItemMapperImpl implements MedicationItemMapper {

    @Override
    public MedicationItem mapEntityToDto(MedicationItemEntity entity) {
        String imageFileName = entity.getImage() != null ? entity.getImage().getName() : null;

        return new MedicationItem(
                entity.getMedicationId(),
                entity.getName(),
                entity.getWeight(),
                entity.getCode(),
                imageFileName
        );
    }
}

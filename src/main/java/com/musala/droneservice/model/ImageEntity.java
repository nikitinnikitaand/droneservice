package com.musala.droneservice.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Represents an image in database.
 */
@Entity
@Table(name = "image")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class ImageEntity {
    /**
     * Identifier of image.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "image_id")
    private Long id;

    /**
     * Content of image in bytes.
     */
    @Lob
    @Column(name = "content")
    private byte[] content;

    /**
     * File name of image.
     */
    @Column(name = "name")
    private String name;

    /**
     * Corresponding medication item.
     */
    @OneToOne(mappedBy = "image")
    private MedicationItemEntity medication;
}

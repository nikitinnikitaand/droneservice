package com.musala.droneservice.mapper.medication;

import com.musala.droneservice.dto.MedicationItem;
import com.musala.droneservice.model.MedicationItemEntity;

/**
 * Service to convert database entities to DTO and vice versa for medication items.
 */
public interface MedicationItemMapper {
    /**
     * Converts given database instance of medication to its DTO representation.
     *
     * @param entity Database instance of medication.
     * @return DTO instance.
     */
    MedicationItem mapEntityToDto(MedicationItemEntity entity);
}

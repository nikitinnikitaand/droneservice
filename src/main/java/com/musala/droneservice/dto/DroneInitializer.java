package com.musala.droneservice.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * Initializer representation of drone. Contains only key data.
 */
@Data
@Getter
@Setter
public class DroneInitializer {
    /**
     * Drone's serial number.
     */
    private String serialNumber;

    /**
     * Drone's model name.
     */
    private String model;

    /**
     * Drone's weight limit in grams.
     */
    private long weightLimit;

    /**
     * Drone's battery percentage level.
     */
    private int batteryCapacityPercentage;
}

package com.musala.droneservice.exception;

/**
 * Exception to be thrown when drone with given identifier has not been found.
 */
public class DroneNotFoundException extends BaseDroneServiceException {
    public DroneNotFoundException(String message) {
        super(message);
    }
}

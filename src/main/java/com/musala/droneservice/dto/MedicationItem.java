package com.musala.droneservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

/**
 * Medication item DTO.
 */
@Data
@Getter
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MedicationItem {
    /**
     * Medication item's identifier.
     */
    private long medicationId;

    /**
     * Medication item's name.
     */
    private String name;

    /**
     * Medication item's total weight.
     */
    private long weight;

    /**
     * Medication item's code.
     */
    private String code;

    /**
     * Medication item's image name.
     */
    private String imageName;
}

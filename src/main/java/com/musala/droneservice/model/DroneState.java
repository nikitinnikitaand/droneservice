package com.musala.droneservice.model;


/**
 * Describes state of drone.
 */
public enum DroneState {
    IDLE,
    LOADING,
    LOADED,
    DELIVERING,
    DELIVERED,
    RETURNING
}

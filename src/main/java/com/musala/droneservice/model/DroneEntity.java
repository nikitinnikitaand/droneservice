package com.musala.droneservice.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Drone representation in database.
 */
@Entity
@Table(name = "drone")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class DroneEntity {
    /**
     * Identifier of drone.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "drone_id")
    private long droneId;

    /**
     * Drone's serial number.
     */
    @Column(name = "serial_number")
    private String serialNumber;

    /**
     * Drone's model name.
     */
    @Column(name = "model")
    private String model;

    /**
     * Drone's weight limit in grams.
     */
    @Column(name = "weight_limit")
    private long weightLimit;

    /**
     * Drone's battery percentage level.
     */
    @Column(name = "battery_capacity_percentage")
    private int batteryCapacityPercentage;

    /**
     * Drone's state.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "state")
    private DroneState state;

    /**
     * Load of medication for current drone.
     */
    @OneToMany(mappedBy = "drone", fetch = FetchType.LAZY)
    private List<MedicationItemEntity> medicationLoad = new ArrayList<>();
}

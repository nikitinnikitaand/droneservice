package com.musala.droneservice.repository;

import com.musala.droneservice.model.MedicationItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository to work with medications from database.
 */
@Repository
public interface MedicationItemEntityRepository extends JpaRepository<MedicationItemEntity, Long> {
}

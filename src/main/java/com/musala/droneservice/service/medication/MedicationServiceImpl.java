package com.musala.droneservice.service.medication;

import com.musala.droneservice.dto.FindAllItemsResponse;
import com.musala.droneservice.dto.MedicationItem;
import com.musala.droneservice.dto.MedicationItemInitializer;
import com.musala.droneservice.exception.CannotUploadImageException;
import com.musala.droneservice.exception.MedicationItemNotFoundException;
import com.musala.droneservice.mapper.medication.MedicationItemMapper;
import com.musala.droneservice.model.ImageEntity;
import com.musala.droneservice.model.MedicationItemEntity;
import com.musala.droneservice.repository.ImageRepository;
import com.musala.droneservice.repository.MedicationItemEntityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementation of service to work with medications.
 */
@Service
@RequiredArgsConstructor
public class MedicationServiceImpl implements MedicationService {
    /**
     * Regular expression to check the name of medication.
     */
    private static final String MEDICATION_NAME_REGEX = "[A-Za-z0-9_-]+";

    /**
     * Regular expression to check the code of medication.
     */
    private static final String MEDICATION_CODE_REGEX = "[A-Z0-9_]+";

    /**
     * Repository to work with medications from database.
     */
    private final MedicationItemEntityRepository medicationItemRepository;

    /**
     * Repository to work with images from database.
     */
    private final ImageRepository imageRepository;

    /**
     * Mapper to convert database entities to DTO and vice versa for medication items.
     */
    private final MedicationItemMapper medicationItemMapper;

    @Override
    public MedicationItem createItem(MedicationItemInitializer medicationItemInitializer) {
        checkInput(medicationItemInitializer);

        MedicationItemEntity medicationItemEntity = new MedicationItemEntity();
        return saveItem(medicationItemEntity, medicationItemInitializer);
    }

    /**
     * Checks correctness of the given data.
     *
     * @param medicationItem Data to check.
     */
    private void checkInput(MedicationItemInitializer medicationItem) {
        // here we will store all error messages
        List<String> errorMessages = new ArrayList<>();
        if (medicationItem.getWeight() < 1) {
            // weight cannot be less than 1
            errorMessages.add("Weight must be greater than zero");
        }

        // check the name of medication to match the expression
        if (medicationItem.getName() != null) {
            String name = medicationItem.getName();
            if (!name.matches(MEDICATION_NAME_REGEX)) {
                errorMessages.add("Name must contain only letters, numbers or symbols '-', '_'");
            }
        } else {
            errorMessages.add("Name is required");
        }

        // check the code of medication to match the expression
        if (medicationItem.getCode() != null) {
            String code = medicationItem.getCode();
            if (!code.matches(MEDICATION_CODE_REGEX)) {
                errorMessages.add("Code must contain only uppercase letters, numbers or symbol '_'");
            }
        } else {
            errorMessages.add("Code is required");
        }

        if (!errorMessages.isEmpty()) {
            // collect all errors and throw the corresponding exception
            throw new IllegalArgumentException(String.join(". ", errorMessages));
        }
    }

    @Override
    public MedicationItem editItem(Long medicationId, MedicationItemInitializer medicationItemInitializer) {
        return saveItem(getMedicationItem(medicationId), medicationItemInitializer);
    }

    /**
     * Saves medication item to database and return DTO instance as a result.
     *
     * @param medicationItemEntity      Entity to save to database.
     * @param medicationItemInitializer Data to fill the database entity.
     * @return DTO instance of saved item.
     */
    private MedicationItem saveItem(MedicationItemEntity medicationItemEntity,
                                    MedicationItemInitializer medicationItemInitializer) {
        medicationItemEntity.setCode(medicationItemInitializer.getCode());
        medicationItemEntity.setName(medicationItemInitializer.getName());
        medicationItemEntity.setWeight(medicationItemInitializer.getWeight());

        MedicationItemEntity savedItem = medicationItemRepository.save(medicationItemEntity);

        return medicationItemMapper.mapEntityToDto(savedItem);
    }

    @Override
    public void deleteItem(Long medicationId) {
        MedicationItemEntity medicationItemEntity = getMedicationItem(medicationId);
        medicationItemRepository.delete(medicationItemEntity);
    }

    @Override
    public void uploadImage(Long medicationId, MultipartFile multipartImage) {
        MedicationItemEntity medicationItem = getMedicationItem(medicationId);

        ImageEntity imageEntity = new ImageEntity();
        try {
            imageEntity.setName(multipartImage.getOriginalFilename());
            imageEntity.setContent(multipartImage.getBytes());
        } catch (IOException exception) {
            throw new CannotUploadImageException(
                    String.format("Cannot upload image for medication with id '%s'", medicationId),
                    exception
            );
        }

        imageEntity.setMedication(medicationItem);
        medicationItem.setImage(imageEntity);

        imageRepository.save(imageEntity);
    }

    /**
     * Tries to get a medication item entity by specified identifier. If this item doesn't exist, then throws
     * corresponding exception.
     *
     * @param medicationId Identifier of medication item to find.
     * @return Found medication item entity.
     */
    private MedicationItemEntity getMedicationItem(Long medicationId) {
        return medicationItemRepository.findById(medicationId)
                .orElseThrow(() ->
                        new MedicationItemNotFoundException(
                                String.format("Medication with id '%s' has not been found", medicationId)
                        )
                );
    }

    @Override
    public FindAllItemsResponse<MedicationItem> getAllMedications(int page, int pageSize, String sortBy,
                                                                  String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();

        Pageable pageable = PageRequest.of(page, pageSize, sort);
        Page<MedicationItemEntity> medicationsPage = medicationItemRepository.findAll(pageable);

        List<MedicationItemEntity> medicationItems = medicationsPage.getContent();

        return new FindAllItemsResponse<>(
                medicationItems.stream().map(medicationItemMapper::mapEntityToDto).collect(Collectors.toList()),
                medicationsPage.getNumber(),
                medicationsPage.getSize(),
                medicationsPage.getTotalElements(),
                medicationsPage.getTotalPages(),
                medicationsPage.isLast()
        );
    }
}

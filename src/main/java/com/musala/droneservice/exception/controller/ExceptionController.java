package com.musala.droneservice.exception.controller;

import com.musala.droneservice.exception.DroneNotFoundException;
import com.musala.droneservice.exception.LowBatteryLevelException;
import com.musala.droneservice.exception.MedicationItemNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.Instant;

/**
 * Controller to handle all exceptions.
 */
@RestControllerAdvice
public class ExceptionController {
    /**
     * Handles bad request exceptions.
     *
     * @param exception Raised exception.
     * @return Error message.
     */
    @ExceptionHandler({IllegalArgumentException.class, LowBatteryLevelException.class})
    public ResponseEntity<ErrorMessage> handleBadRequest(Exception exception) {
        return composeMessage(HttpStatus.BAD_REQUEST, exception);
    }

    /**
     * Handles not request exceptions.
     *
     * @param exception Raised exception.
     * @return Error message.
     */
    @ExceptionHandler({DroneNotFoundException.class, MedicationItemNotFoundException.class})
    public ResponseEntity<ErrorMessage> handleNotFound(Exception exception) {
        return composeMessage(HttpStatus.NOT_FOUND, exception);

    }

    /**
     * Handles any unhanded exceptions.
     *
     * @param exception Raised exception.
     * @return Error message.
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessage> handleAnyException(Exception exception) {
        return composeMessage(HttpStatus.INTERNAL_SERVER_ERROR, exception);
    }

    /**
     * Composes error message to send back to client.
     *
     * @param status    Status of error message.
     * @param exception Corresponding exception.
     * @return Error message.
     */
    private ResponseEntity<ErrorMessage> composeMessage(HttpStatus status, Exception exception) {
        return ResponseEntity.status(status.value())
                .body(new ErrorMessage(Instant.now(), status.value(), exception.getMessage()));
    }
}

package com.musala.droneservice.controller;

import com.musala.droneservice.dto.FindAllItemsResponse;
import com.musala.droneservice.dto.MedicationItem;
import com.musala.droneservice.dto.MedicationItemInitializer;
import com.musala.droneservice.service.medication.MedicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Controller to work with drones.
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/medication")
public class MedicationController {
    /**
     * Service to work with medications.
     */
    private final MedicationService medicationService;

    /**
     * Creates medication item with given data.
     *
     * @param medicationItemInitializer Data to create medication item.
     * @return Created medication item.
     */
    @PostMapping
    public ResponseEntity<MedicationItem> createItem(@RequestBody MedicationItemInitializer medicationItemInitializer) {
        return ResponseEntity.ok(medicationService.createItem(medicationItemInitializer));
    }

    /**
     * Uploads an image to the given medication item.
     *
     * @param medicationId   Identifier of mediation to upload to.
     * @param multipartImage Image to be uploaded.
     */
    @PostMapping("/{medicationId}/image")
    public void uploadImage(@PathVariable Long medicationId, @RequestParam MultipartFile multipartImage) {
        medicationService.uploadImage(medicationId, multipartImage);
    }

    /**
     * Edits the medication to change its data.
     *
     * @param medicationId              Identifier of medication to update.
     * @param medicationItemInitializer New data to update.
     * @return Updated medication item.
     */
    @PutMapping("/{medicationId}")
    public ResponseEntity<MedicationItem> editItem(@PathVariable Long medicationId,
                                                   @RequestBody MedicationItemInitializer medicationItemInitializer) {
        return ResponseEntity.ok(medicationService.editItem(medicationId, medicationItemInitializer));
    }

    /**
     * Deletes medication item by its identifier.
     *
     * @param medicationId Identifier of medication to delete.
     */
    @DeleteMapping("/{medicationId}")
    public void deleteItem(@PathVariable Long medicationId) {
        medicationService.deleteItem(medicationId);
    }

    /**
     * Searches for medication items with specified parameters.
     *
     * @param page          Number of page to load.
     * @param pageSize      Total size of pages.
     * @param sortBy        The name of field to sort the result.
     * @param sortDirection Direction to sort the result (ASC or DESC)
     * @return Response of found medication items.
     */
    @GetMapping
    public ResponseEntity<FindAllItemsResponse<MedicationItem>> getAllMedications(
            @RequestParam(value = "page", defaultValue = "0", required = false) int page,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = "medicationId", required = false) String sortBy,
            @RequestParam(value = "sortDirection", defaultValue = "asc", required = false) String sortDirection
    ) {
        return ResponseEntity.ok(medicationService.getAllMedications(page, pageSize, sortBy, sortDirection));
    }
}

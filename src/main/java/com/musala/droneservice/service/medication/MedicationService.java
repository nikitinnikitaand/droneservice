package com.musala.droneservice.service.medication;

import com.musala.droneservice.dto.FindAllItemsResponse;
import com.musala.droneservice.dto.MedicationItem;
import com.musala.droneservice.dto.MedicationItemInitializer;
import org.springframework.web.multipart.MultipartFile;

/**
 * Service to work with medications.
 */
public interface MedicationService {
    /**
     * Creates medication item with given data.
     *
     * @param medicationItemInitializer Data to create medication item.
     * @return Created medication item.
     */
    MedicationItem createItem(MedicationItemInitializer medicationItemInitializer);

    /**
     * Edits the medication to change its data.
     *
     * @param medicationId              Identifier of medication to update.
     * @param medicationItemInitializer New data to update.
     * @return Updated medication item.
     */
    MedicationItem editItem(Long medicationId, MedicationItemInitializer medicationItemInitializer);

    /**
     * Deletes medication item by its identifier.
     *
     * @param medicationId Identifier of medication to delete.
     */
    void deleteItem(Long medicationId);

    /**
     * Uploads an image to the given medication item.
     *
     * @param medicationId   Identifier of mediation to upload to.
     * @param multipartImage Image to be uploaded.
     */
    void uploadImage(Long medicationId, MultipartFile multipartImage);

    /**
     * Searches for medication items with specified parameters.
     *
     * @param page          Number of page to load.
     * @param pageSize      Total size of pages.
     * @param sortBy        The name of field to sort the result.
     * @param sortDirection Direction to sort the result (ASC or DESC)
     * @return Response of found medication items.
     */
    FindAllItemsResponse<MedicationItem> getAllMedications(int page, int pageSize, String sortBy, String sortDirection);
}

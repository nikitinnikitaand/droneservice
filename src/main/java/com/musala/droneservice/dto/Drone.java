package com.musala.droneservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.musala.droneservice.model.DroneState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.util.List;

/**
 * Drone DTO.
 */
@Data
@Getter
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Drone {
    /**
     * Drone's identifier.
     */
    private long droneId;

    /**
     * Drone's serial number.
     */
    private String serialNumber;

    /**
     * Drone's model name.
     */
    private String model;

    /**
     * Drone's weight limit in grams.
     */
    private long weightLimit;

    /**
     * Drone's battery percentage level.
     */
    private int batteryCapacityPercentage;

    /**
     * Drone's state.
     */
    private DroneState state;

    /**
     * Load of medication for current drone.
     */
    private List<MedicationItem> medicationLoad;
}

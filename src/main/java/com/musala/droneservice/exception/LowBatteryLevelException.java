package com.musala.droneservice.exception;

/**
 * Exception to be thrown when drone is running out of battery level.
 */
public class LowBatteryLevelException extends BaseDroneServiceException {
    public LowBatteryLevelException(String message) {
        super(message);
    }
}

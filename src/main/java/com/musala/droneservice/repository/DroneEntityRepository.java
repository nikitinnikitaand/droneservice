package com.musala.droneservice.repository;

import com.musala.droneservice.model.DroneEntity;
import com.musala.droneservice.model.DroneState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * Repository to work with drones from database.
 */
@Repository
public interface DroneEntityRepository extends JpaRepository<DroneEntity, Long> {
    /**
     * Searches for drones with predefined battery level and in given states.
     *
     * @param batteryCapacityPercentage Battery level capacity to search for.
     * @param states                    States of drone to search for.
     * @return Found drones with given parameters.
     */
    List<DroneEntity> findByBatteryCapacityPercentageGreaterThanAndStateIn(int batteryCapacityPercentage,
                                                                           Collection<DroneState> states);

    /**
     * Searches for drones with predefined battery level.
     *
     * @param batteryCapacityPercentage Battery level capacity to search for.
     * @return Found drones with given parameters.
     */
    List<DroneEntity> findByBatteryCapacityPercentageLessThan(int batteryCapacityPercentage);
}

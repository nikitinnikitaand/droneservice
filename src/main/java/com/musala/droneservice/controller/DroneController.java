package com.musala.droneservice.controller;

import com.musala.droneservice.dto.*;
import com.musala.droneservice.service.drone.DroneService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * Controller to work with drones.
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/drone")
public class DroneController {
    /**
     * Service to work with drones.
     */
    private final DroneService droneService;

    /**
     * Creates new drone with given data.
     *
     * @param droneInitializer Drone's data to register.
     * @return Created drone entity.
     */
    @PostMapping("/register")
    public ResponseEntity<Drone> registerDrone(@RequestBody DroneInitializer droneInitializer) {
        return ResponseEntity.ok(droneService.registerDrone(droneInitializer));
    }

    /**
     * Searches for available drones for loading. Drone is considered as available for loading when
     * it's in IDLE or LOADING state and its battery level is not less than 25.
     *
     * @return List of available drones.
     */
    @GetMapping("/available")
    public ResponseEntity<List<Drone>> getAvailableDronesToLoad() {
        return ResponseEntity.ok(droneService.getAvailableDronesToLoad());
    }

    /**
     * Loads specified drone with some medications.
     *
     * @param droneId       Identifier of drone to be loaded.
     * @param medicationIds Set of unique identifiers to load medications to the drone.
     * @return Loaded drone with medications.
     */
    @PostMapping("/{droneId}/load")
    public ResponseEntity<Drone> loadDrone(@PathVariable Long droneId, @RequestBody Set<Long> medicationIds) {
        return ResponseEntity.ok(droneService.loadDrone(droneId, medicationIds));
    }

    /**
     * Checks drone loading and returns medications of current drone.
     *
     * @param droneId Identifier of drone to check loading.
     * @return A list of medications for the current drone.
     */
    @GetMapping("/{droneId}/load")
    public ResponseEntity<List<MedicationItem>> checkDroneLoad(@PathVariable Long droneId) {
        return ResponseEntity.ok(droneService.checkDroneLoad(droneId));
    }

    /**
     * Checks a battery level of specified drone.
     *
     * @param droneId Identifier of drone to check battery level.
     * @return Battery percentage of current drone.
     */
    @GetMapping("/{droneId}/battery")
    public ResponseEntity<Integer> checkDroneBatteryLevel(@PathVariable Long droneId) {
        return ResponseEntity.ok(droneService.checkDroneBatteryLevel(droneId));
    }

    /**
     * Searches for drones with specified parameters.
     *
     * @param page          Number of page to load.
     * @param pageSize      Total size of pages.
     * @param sortBy        The name of field to sort the result.
     * @param sortDirection Direction to sort the result (ASC or DESC)
     * @return Response of found drones.
     */
    @GetMapping
    public ResponseEntity<FindAllItemsResponse<Drone>> getAllDrones(
            @RequestParam(value = "page", defaultValue = "0", required = false) int page,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = "droneId", required = false) String sortBy,
            @RequestParam(value = "sortDirection", defaultValue = "asc", required = false) String sortDirection
    ) {
        return ResponseEntity.ok(droneService.getAllDrones(page, pageSize, sortBy, sortDirection));
    }
}

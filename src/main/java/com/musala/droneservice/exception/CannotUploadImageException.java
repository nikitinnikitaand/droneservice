package com.musala.droneservice.exception;

/**
 * Exception to be thrown when something went wrong while uploading an image.
 */
public class CannotUploadImageException extends BaseDroneServiceException {
    public CannotUploadImageException(String message, Exception exception) {
        super(message, exception);
    }
}

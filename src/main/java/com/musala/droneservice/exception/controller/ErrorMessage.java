package com.musala.droneservice.exception.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

/**
 * Describes an error message.
 */
@Getter
@Setter
@AllArgsConstructor
public class ErrorMessage {
    private Instant timestamp;
    private Integer status;
    private String error;
}

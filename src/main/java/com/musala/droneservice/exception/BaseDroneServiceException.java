package com.musala.droneservice.exception;

/**
 * Base exception for the service.
 */
public abstract class BaseDroneServiceException extends RuntimeException {
    public BaseDroneServiceException(String message) {
        super(message);
    }

    public BaseDroneServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}

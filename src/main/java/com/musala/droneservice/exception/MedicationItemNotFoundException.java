package com.musala.droneservice.exception;

/**
 * Exception to be thrown when medication item with given identifier has not been found.
 */
public class MedicationItemNotFoundException extends BaseDroneServiceException {
    public MedicationItemNotFoundException(String message) {
        super(message);
    }
}

package com.musala.droneservice.service.drone;

import com.musala.droneservice.dto.*;

import java.util.List;
import java.util.Set;

/**
 * Service to work with drones (load, find, check etc.)
 */
public interface DroneService {
    /**
     * Creates new drone with given data.
     *
     * @param droneInitializer Drone's data to register.
     * @return Created drone entity.
     */
    Drone registerDrone(DroneInitializer droneInitializer);

    /**
     * Searches for available drones for loading. Drone is considered as available for loading when
     * it's in IDLE or LOADING state and its battery level is not less than 25.
     *
     * @return List of available drones.
     */
    List<Drone> getAvailableDronesToLoad();

    /**
     * Loads specified drone with some medications.
     *
     * @param droneId       Identifier of drone to be loaded.
     * @param medicationIds Set of unique identifiers to load medications to the drone.
     * @return Loaded drone with medications.
     */
    Drone loadDrone(Long droneId, Set<Long> medicationIds);

    /**
     * Checks drone loading and returns medications of current drone.
     *
     * @param droneId Identifier of drone to check loading.
     * @return A list of medications for the current drone.
     */
    List<MedicationItem> checkDroneLoad(Long droneId);

    /**
     * Checks a battery level of specified drone.
     *
     * @param droneId Identifier of drone to check battery level.
     * @return Battery percentage of current drone.
     */
    Integer checkDroneBatteryLevel(Long droneId);

    /**
     * Searches for drones with specified parameters.
     *
     * @param page          Number of page to load.
     * @param pageSize      Total size of pages.
     * @param sortBy        The name of field to sort the result.
     * @param sortDirection Direction to sort the result (ASC or DESC)
     * @return Response of found drones.
     */
    FindAllItemsResponse<Drone> getAllDrones(int page, int pageSize, String sortBy, String sortDirection);
}

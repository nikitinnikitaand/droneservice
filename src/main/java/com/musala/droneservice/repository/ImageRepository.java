package com.musala.droneservice.repository;

import com.musala.droneservice.model.ImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository to work with images from database.
 */
@Repository
public interface ImageRepository extends JpaRepository<ImageEntity, Long> {
}

package com.musala.droneservice.service.drone;

import com.musala.droneservice.dto.Drone;
import com.musala.droneservice.dto.DroneInitializer;
import com.musala.droneservice.dto.FindAllItemsResponse;
import com.musala.droneservice.dto.MedicationItem;
import com.musala.droneservice.exception.DroneNotFoundException;
import com.musala.droneservice.exception.LowBatteryLevelException;
import com.musala.droneservice.mapper.medication.MedicationItemMapper;
import com.musala.droneservice.model.DroneEntity;
import com.musala.droneservice.model.DroneState;
import com.musala.droneservice.model.MedicationItemEntity;
import com.musala.droneservice.repository.DroneEntityRepository;
import com.musala.droneservice.repository.MedicationItemEntityRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Implementation of service to work with drones (load, find, check etc.)
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class DroneServiceImpl implements DroneService {
    /**
     * Template of error message when something went wrong while loading a drone.
     */
    private static final String LOAD_MESSAGE_ERROR_TEMPLATE = "Cannot load drone with id '%s'. Current weight of " +
            "medications ('%s') is greater than the allowable load of the drone: '%s'";

    /**
     * Repository to work with medications in database.
     */
    private final MedicationItemEntityRepository medicationItemRepository;

    /**
     * Repository to work with drones in database.
     */
    private final DroneEntityRepository droneEntityRepository;

    /**
     * Mapper to convert database entities to DTO and vice versa for medication items.
     */
    private final MedicationItemMapper medicationItemMapper;

    @Override
    public Drone registerDrone(DroneInitializer droneInitializer) {
        // firstly, check the input data to be correct
        checkInput(droneInitializer);

        // create new drone entity with IDLE state by default
        DroneEntity droneEntity = new DroneEntity();
        droneEntity.setModel(droneInitializer.getModel());
        droneEntity.setSerialNumber(droneInitializer.getSerialNumber());
        droneEntity.setWeightLimit(droneInitializer.getWeightLimit());
        droneEntity.setBatteryCapacityPercentage(droneInitializer.getBatteryCapacityPercentage());
        droneEntity.setState(DroneState.IDLE);
        droneEntity.setMedicationLoad(List.of());

        DroneEntity savedDrone = droneEntityRepository.save(droneEntity);

        return toDroneDto(savedDrone);
    }

    @Override
    public List<Drone> getAvailableDronesToLoad() {
        // get only drones in IDLE or LOADING state and with battery level greater than 24
        return droneEntityRepository.findByBatteryCapacityPercentageGreaterThanAndStateIn(
                        24,
                        List.of(DroneState.IDLE, DroneState.LOADING)
                ).stream()
                .map(this::toDroneDto)
                .collect(Collectors.toList());
    }

    /**
     * Checks correctness of the given data.
     *
     * @param droneInitializer Data to check.
     */
    private void checkInput(DroneInitializer droneInitializer) {
        // here we will store all error messages
        List<String> errorMessages = new ArrayList<>();
        if (!StringUtils.hasLength(droneInitializer.getModel())) {
            errorMessages.add("Model field is required");
        }

        // check that drone's serial number exists and not empty
        if (StringUtils.hasLength(droneInitializer.getSerialNumber())) {
            String serialNumber = droneInitializer.getSerialNumber();

            // if so, checks that serial number's size not greater than 100
            if (serialNumber.length() > 100) {
                // if not, saves error message
                errorMessages.add("Serial number field must be no more than 100 characters");
            }
        } else {
            // if not, saves error message
            errorMessages.add("Serial number field is required");
        }

        // checks that drone's weight limit greater than zero and less than or equal to 500
        long weightLimit = droneInitializer.getWeightLimit();
        if (weightLimit < 0 || weightLimit > 500) {
            errorMessages.add("Weight limit must be in range [0, 500] gr");
        }

        // then, checks that battery percentage greater than zero and less than or equal to 100
        int batteryPercentage = droneInitializer.getBatteryCapacityPercentage();
        if (batteryPercentage < 0 || batteryPercentage > 100) {
            errorMessages.add("Battery percentage must be in range [0, 100]");
        }

        if (!errorMessages.isEmpty()) {
            // collect all errors and throw the corresponding exception
            throw new IllegalArgumentException(String.join(". ", errorMessages));
        }
    }

    @Override
    public Drone loadDrone(Long droneId, Set<Long> medicationIds) {
        if (medicationIds.isEmpty()) {
            throw new IllegalArgumentException("Cannot load drone with empty medications");
        }

        // check that drone exists and get an instance
        DroneEntity droneEntity = getDroneEntity(droneId);

        // check the battery level, if it's low than 25, then this drone cannot be loaded
        if (droneEntity.getBatteryCapacityPercentage() < 25) {
            throw new LowBatteryLevelException(
                    String.format(
                            "Drone with id '%s' has low level of battery ('%s') and cannot be loaded",
                            droneId,
                            droneEntity.getBatteryCapacityPercentage()
                    )
            );
        }

        // then, if current drone in IDLE state, change its state to LOADING
        if (droneEntity.getState() == DroneState.IDLE) {
            droneEntity.setState(DroneState.LOADING);
        }

        // and if the drone is not in LOADING state, so this drone cannot be loaded
        if (droneEntity.getState() != DroneState.LOADING) {
            throw new IllegalArgumentException(
                    String.format(
                            "Cannot load drone with id '%s', because it's in '%s' state",
                            droneId,
                            droneEntity.getState()
                    )
            );
        }

        // get all medications with provided identifiers
        List<MedicationItemEntity> foundMedications = new ArrayList<>(medicationItemRepository.findAllById(medicationIds));

        // then checks, that all medications have been found
        if (foundMedications.isEmpty() || medicationIds.size() != foundMedications.size()) {
            // otherwise, try to find an absent medications and throw corresponding exception
            Set<Long> foundMedicationIds = foundMedications.stream()
                    .map(MedicationItemEntity::getMedicationId)
                    .collect(Collectors.toSet());

            Set<Long> notFoundMedicationIds = new HashSet<>(medicationIds);
            notFoundMedicationIds.removeAll(foundMedicationIds);

            throw new IllegalArgumentException(
                    String.format(
                            "Cannot find medication items with following identifiers: '%s'",
                            notFoundMedicationIds.stream().map(Object::toString).collect(Collectors.joining(", "))
                    )
            );
        }

        // calculates total current load of the drone in terms of weight: the weight of already loaded medications +
        // new weight
        Long newLoad = foundMedications.stream().map(MedicationItemEntity::getWeight).reduce(0L, Long::sum);
        Long alreadyLoaded = droneEntity.getMedicationLoad()
                .stream()
                .map(MedicationItemEntity::getWeight)
                .reduce(0L, Long::sum);

        long totalLoad = newLoad + alreadyLoaded;

        // if it's too much to the current drone, throw corresponding exception
        if (totalLoad > droneEntity.getWeightLimit()) {
            throw new IllegalArgumentException(
                    String.format(LOAD_MESSAGE_ERROR_TEMPLATE, droneId, totalLoad, droneEntity.getWeightLimit())
            );
        }

        // if all checks went well, then we start processing
        if (totalLoad == droneEntity.getWeightLimit()) {
            // if current weight total weight is equal to the drone's max weight, then this drone cannot be
            // loaded with new medications. So, change its state to LOADED
            droneEntity.setState(DroneState.LOADED);
        }

        // then, add each medication to the drone
        foundMedications.forEach(medicationItem -> {
            droneEntity.getMedicationLoad().add(medicationItem);
            medicationItem.setDrone(droneEntity);
        });

        // and save it
        return toDroneDto(droneEntityRepository.save(droneEntity));
    }

    @Override
    public List<MedicationItem> checkDroneLoad(Long droneId) {
        return getDroneEntity(droneId).getMedicationLoad().stream()
                .map(medicationItemMapper::mapEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public Integer checkDroneBatteryLevel(Long droneId) {
        return getDroneEntity(droneId).getBatteryCapacityPercentage();
    }

    /**
     * Tries to get a drone entity by specified identifier. If this drone doesn't exist, then throws corresponding
     * exception.
     *
     * @param droneId Identifier of drone to find a drone.
     * @return Found drone entity.
     */
    private DroneEntity getDroneEntity(Long droneId) {
        return droneEntityRepository.findById(droneId)
                .orElseThrow(() ->
                        new DroneNotFoundException(
                                String.format("Drone with id '%s' has not been found", droneId)
                        )
                );
    }

    @Override
    public FindAllItemsResponse<Drone> getAllDrones(int page, int pageSize, String sortBy, String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();

        Pageable pageable = PageRequest.of(page, pageSize, sort);
        Page<DroneEntity> dronesPage = droneEntityRepository.findAll(pageable);

        List<DroneEntity> drones = dronesPage.getContent();

        return new FindAllItemsResponse<>(
                drones.stream().map(this::toDroneDto).collect(Collectors.toList()),
                dronesPage.getNumber(),
                dronesPage.getSize(),
                dronesPage.getTotalElements(),
                dronesPage.getTotalPages(),
                dronesPage.isLast()
        );
    }

    /**
     * Converts current drone entity from database to DTO instance.
     *
     * @param droneEntity Drone entity from database.
     * @return Drone DTO instance.
     */
    private Drone toDroneDto(DroneEntity droneEntity) {
        return new Drone(
                droneEntity.getDroneId(),
                droneEntity.getSerialNumber(),
                droneEntity.getModel(),
                droneEntity.getWeightLimit(),
                droneEntity.getBatteryCapacityPercentage(),
                droneEntity.getState(),
                droneEntity.getMedicationLoad()
                        .stream()
                        .map(medicationItemMapper::mapEntityToDto)
                        .collect(Collectors.toList())
        );
    }

    /**
     * Periodically find drones with low battery level and log WARN.
     */
    @Scheduled(fixedDelay = 10000)
    public void checkDronesWithLowBattery() {
        droneEntityRepository.findByBatteryCapacityPercentageLessThan(25).forEach(drone ->
                log.warn(
                        "Drone with id '{}' has low battery level - '{}'",
                        drone.getDroneId(),
                        drone.getBatteryCapacityPercentage()
                )
        );
    }
}

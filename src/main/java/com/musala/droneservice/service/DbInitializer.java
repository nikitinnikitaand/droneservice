package com.musala.droneservice.service;

import com.musala.droneservice.model.DroneEntity;
import com.musala.droneservice.model.DroneState;
import com.musala.droneservice.model.ImageEntity;
import com.musala.droneservice.model.MedicationItemEntity;
import com.musala.droneservice.repository.DroneEntityRepository;
import com.musala.droneservice.repository.MedicationItemEntityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Utility class to fill the database with data before starting the application.
 */
@Service
@RequiredArgsConstructor
public class DbInitializer {
    /**
     * Repository to work with medications in database.
     */
    private final MedicationItemEntityRepository medicationItemRepository;

    /**
     * Repository to work with drones in database.
     */
    private final DroneEntityRepository droneEntityRepository;

    /**
     * Fills the database with data when the application event ready event is received.
     */
    @EventListener
    public void onApplicationReady(ApplicationReadyEvent ready) {
        // loaded drone
        DroneEntity loadedDrone = new DroneEntity();
        loadedDrone.setState(DroneState.LOADED);
        loadedDrone.setModel("Middleweight");
        loadedDrone.setWeightLimit(600);
        loadedDrone.setSerialNumber("111111");
        loadedDrone.setBatteryCapacityPercentage(100);

        MedicationItemEntity medicationItemWithImage = new MedicationItemEntity();
        medicationItemWithImage.setDrone(loadedDrone);
        medicationItemWithImage.setWeight(150);
        medicationItemWithImage.setCode("222222");
        medicationItemWithImage.setName("medication_with_image");

        ImageEntity imageEntity = new ImageEntity();
        imageEntity.setName("image");
        imageEntity.setContent(new byte[]{});
        imageEntity.setMedication(medicationItemWithImage);

        medicationItemWithImage.setImage(imageEntity);

        List<MedicationItemEntity> medicationLoadForFirstDrone = List.of(
                new MedicationItemEntity("Medication1", 150, "333333", loadedDrone),
                new MedicationItemEntity("Medication2", 150, "444444", loadedDrone),
                new MedicationItemEntity("Medication3", 150, "555555", loadedDrone)
        );

        loadedDrone.getMedicationLoad().addAll(medicationLoadForFirstDrone);
        loadedDrone.getMedicationLoad().add(medicationItemWithImage);

        // IDLE drone without medication
        DroneEntity idleDrone = new DroneEntity();
        idleDrone.setState(DroneState.IDLE);
        idleDrone.setModel("Middleweight");
        idleDrone.setWeightLimit(500);
        idleDrone.setSerialNumber("asd3434gg");
        idleDrone.setBatteryCapacityPercentage(100);

        // Free medication that can be loaded via API
        List<MedicationItemEntity> freeMedications = List.of(
                new MedicationItemEntity("Medication4", 100, "666666"),
                new MedicationItemEntity("Medication5", 250, "777777"),
                new MedicationItemEntity("Medication6", 550, "888888")
        );

        medicationItemRepository.saveAll(freeMedications);

        // partially loaded drone (loading in progress)
        DroneEntity loadingDrone = new DroneEntity();
        loadingDrone.setState(DroneState.LOADING);
        loadingDrone.setModel("Cruiserweight");
        loadingDrone.setWeightLimit(1000);
        loadingDrone.setSerialNumber("654431");
        loadingDrone.setBatteryCapacityPercentage(73);

        List<MedicationItemEntity> medicationLoadForThirdDrone = List.of(
                new MedicationItemEntity("Medication7", 150, "9999999", loadingDrone),
                new MedicationItemEntity("Medication8", 550, "10101010", loadingDrone)
        );

        loadingDrone.getMedicationLoad().addAll(medicationLoadForThirdDrone);

        // delivering drone
        DroneEntity deliveringDrone = new DroneEntity();
        deliveringDrone.setState(DroneState.DELIVERING);
        deliveringDrone.setModel("Heavyweight");
        deliveringDrone.setWeightLimit(2000);
        deliveringDrone.setSerialNumber("91827364");
        deliveringDrone.setBatteryCapacityPercentage(68);

        List<MedicationItemEntity> medicationLoadForFourthDrone = List.of(
                new MedicationItemEntity("Medication9", 150, "19191919", deliveringDrone),
                new MedicationItemEntity("Medication10", 550, "18181818", deliveringDrone),
                new MedicationItemEntity("Medication11", 1300, "17171717", deliveringDrone)
        );

        deliveringDrone.getMedicationLoad().addAll(medicationLoadForFourthDrone);

        // delivered drone
        DroneEntity deliveredDrone = new DroneEntity();
        deliveredDrone.setState(DroneState.DELIVERED);
        deliveredDrone.setModel("Lightweight");
        deliveredDrone.setWeightLimit(200);
        deliveredDrone.setSerialNumber("7666777");
        deliveredDrone.setBatteryCapacityPercentage(48);

        List<MedicationItemEntity> medicationLoadForFifthDrone = List.of(
                new MedicationItemEntity("Medication12", 200, "16161616", deliveredDrone)
        );

        deliveredDrone.getMedicationLoad().addAll(medicationLoadForFifthDrone);

        // returning drone
        DroneEntity returningDrone = new DroneEntity();
        returningDrone.setState(DroneState.RETURNING);
        returningDrone.setModel("Lightweight");
        returningDrone.setWeightLimit(200);
        returningDrone.setSerialNumber("7666777");
        returningDrone.setBatteryCapacityPercentage(38);

        // low level drone
        DroneEntity lowLevelDrone = new DroneEntity();
        lowLevelDrone.setState(DroneState.IDLE);
        lowLevelDrone.setModel("Lightweight");
        lowLevelDrone.setWeightLimit(200);
        lowLevelDrone.setSerialNumber("4555554");
        lowLevelDrone.setBatteryCapacityPercentage(17);

        droneEntityRepository.saveAll(
                List.of(loadedDrone, idleDrone, loadingDrone, deliveringDrone, deliveredDrone, returningDrone, lowLevelDrone)
        );

        medicationItemRepository.save(medicationItemWithImage);
        medicationItemRepository.saveAll(medicationLoadForFirstDrone);
        medicationItemRepository.saveAll(medicationLoadForThirdDrone);
        medicationItemRepository.saveAll(medicationLoadForFourthDrone);
        medicationItemRepository.saveAll(medicationLoadForFifthDrone);
    }
}
